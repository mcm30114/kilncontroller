# Kiln Controller / Kiln Tracker #



Rest of the documentation can be found in the repo.

## Repo Structure

### docs

Source for all documentation assets.  Diagrams, Drawings, 

### bin

Location for any binary assets that are results of any compiles, or any dependencies 

### src

Location for any source files that would be used to directly create the application.  Images, code resources, data structures.

## Repo Purpose

### What is this repository for? ###

#### Quick summary

This is a work in progress to start the creation of an app / collection of hardware needed to track the performance of a Kiln over a set of firings and lifespan.  Key high level activities:

* Kiln status and info (per kiln)
* firing info (per kiln)
* Lifecycle (per kiln) and costs tracking (per firing) for issues of maintenance and chargebacks to a project
* Maintenance schedules (Lifespan of elements, relays, kiln wash or repair updates, etc)
* Firing progress monitoring -- potentially using Arduino like controllers, involvement of a mobile app (for progress and alerts), possibly some remote work involving a hosted server for data collection, analysis, and push notifications. 
* Cone ratings to Temperature conversions / look ups (C & F)
* Tracking Firing Schedules and the related programming of that schedule per controller type (Autofire / Bartlett)
* Integration with Orton Autofire or Bartlett V6-CF's may come later. 

#### Version

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

( Will be defined as the project matures )
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

( Will be defined as the project matures )
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin -- Michael Martin // mcm30114-at-gmail-com

* Other community or team contact