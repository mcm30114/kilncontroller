# Kiln Tracker #

## Summary: ## 
Create a system where a person who needs to manage kiln firings can do so and get historical information on firings.

## Overview: ##
A person / artist that uses a kiln typically works in glass or ceramic media.  Proper kiln operation is critical to the success of the production process. Historical tracking is good for tracking the performance of a given kiln, or the performance of using a particular media in a kiln.

There are many different factors to the kiln firing process, some of which are:
	- Kind of Kiln
	- Kind of Media
	- Firing type
	- Firing schedule
	- Life of Elements
	- Life of Relays (in electronic controller board based kilns)

Examples of Kilns types and characteristics are:
	- Ceramic
	- Glass (specific coil configuration)
	- Volume of the kiln (H, W, D)
	- control type ( Manual, Kiln Sitter, Computer Controlled)
	- Loading orientation
	- Power supply needs (single phase, three phase, 120-208v)

Examples of Media types:
	- Stoneware
	- Porcelain
	- Glass beads / lamp work
	- Glass General
	- Jewelry (glass, ceramic, mixed media)
	- Clays of different firing requirements (high fire, low fire, "cone" temp)

Examples of Firing Types:
	- Ceramic:
		- Bisque
		- Glazing
		- Porcelain
		- Raku
	- Glass:
		- Annealing
		- Fusing
		- Slumping
		- Tacking

Examples of Firing Schedules:
	- Firing pattern for Ceramics
		- Type of clay, type of kiln
	- Firing Patterns for Glazing
	- Firing Patterns for fusing
	- Firing Patterns for Annealing
	- Firing Patterns for slumping
	- Firing patterns of above main categories with the added permuotation of firing load size (% of kiln loaded, mass of content to be fired, layers of glass to be fused, etc)

## Mobile Application Description: ##

The first iteration of the mobile application will allow the artist to start the process of setting up the firing to be tracked, track events, record actions, and then have a complete summary of that firing.  Simple tracking of dates, times, actions, temperature recordings would be captured.

Other iterations would add:
	- Deeper information around the firing set up
		- Type of firing, type of media, before and after pictures
	- Automation integration
		- Thermocouple monitoring and reporting of tracking using a microcontroller (Raspberry Pi, Arduino)
	- Weather and environmental conditions
		- ambient temperature and humidity
		- general weather conditions
			- sunny, overcast, raining, etc.
	- Centralized data collection posted to a server for long term data storage, analysis, and interactions between mobile, desktop, and web applications.
	- Exports and imports of data
	- Push notification of events
	- Alarms

### Key Screens: ###
	Phase 1
		- Firing Set up screen
		- Firing History List screen
		- Event Data Entry
		- Event Editing of an entry
		- Status screen of active firings

	Phase 2
		- Start of firing summary
		- End of firing summary
		- Before and After pictures
		- Environmental recordings (temp, humidity)

	Phase 3
		- Firing Types
		- FIring Schedules
		- Push Notifications

Kiln Attributes:
	- Manufacturer
	- Type
	- Model Number
	- Serial Number
	- Controller type
	- volume (cu ft) 
	- Power connections
	- Power Voltage
	- Power Amps
	- Power KWH rating

Kiln Manufacturer Atributes:
	- Manufacturer
	- Support #
	- Support email
	- Website

Event Log:
	( Firings )
	- Start Time / Date
	- End Time / Date
	- Kiln used
	- Controller used (manual, Autofire, Skutt, Bartlett)
	- Media Used {Glass, Ceramic, Glaze}
	- Temperature Recorded
	- Actions Taken
		- Lid position
		- Power Switch Position
		- Peep holes open / closed
	- Firing Sequence (s) [ See FIRING SCHEDULE below]
	- Notes

	( Service )
	- Time / Date
	- Kiln used
	- Service performed
	- Notes

Load:
	- Date / time
	- Description / title
	- start conditions
		- Environmental
		- Load Density
			- Ceramic : {Light, Medium, Full}
			- Glass : {one, two, three, four - layer of glass}
	- {Events}
	- End Condition
		- Environmental
	- Cone Attempted
	- Pictures of before / after

Firing Types:
	- Ceramic:
		- Bisque
		- Glaze
		- Raku
		- Porcelain
	- Glass:
		- Annealing
		- Slumping
		- Fusing
		- Tacking
	- Jewelry

Firing Schedule:
	- Firing Type
	- Kiln Type
	- Firing Sequence Description / Notes
	- Sequence:
		- Sequence Number
		- Sequence Name
		- Relative Time event Start
		- Temperature at start
		- Kiln Physical Actions
			- Temp Setting (.. Set Temp / dials to .. )
			- Lid or Vents Open / Closed
		- Target Temp (Cone / C / F)
		- Ramp Rate of Temp change per hour
		- Soak time
		- Expected Temp
		- Temperature at end
		- Relative Time event end
		- Event Duration

Maintenance Schedule:
	- Kiln
	- Service Hours 
		- On elements
		- On Relays
	- Calendar period
	- Suggested Service


