# Kiln Log Epics, Stories, Requirements

## == Phase 1 == 

Story:  As a user of the app, I want to be able to record the events and details around one firing.

Process:

Quick steps:  

   1) User prepares a log entry as they are preparing for a firing.  A data page / report page can be created and stored prior to the actual firing activity.   
   
   2) User loads the kiln, recalls the Log entry main page, and selects START FIRING.  Sequence that was set up then starts to count down and execute.  
   
   3) User is either prompted by the application to take readings at a time interval (manual firing) or is prompted when a segment of the firing sequence is complete and needs further action.  Counters keep going.  Data entry is taken at these intervals.
   
4) Firing comes to an end, kiln is then set to rest for a period of time and an ETA of temperature equalization is calculated or displayed as captured in the program. 

5) When the calculated end of the firing is complete, user is alerted by the application.  Application stops counting.  Data entry can continue for a post-firing log of actual results. 

6) User can then recall at a later time what the expected inputs to the firing are, what the steps were that were taken to obtain the results, and then a realization of the results post firing. 


## Kiln Load Firing

#### Start load
The user launches the app, and is shown a landing screen.  The landing screen will show either statuses of firings in progress or a list of past firings.  The current state of the landing page is not part of this current User Story and will be covered in a separate story. 

User selects ADD a firing, a subsequent screen appears.  On this NEW FIRING screen, there are fields:

- Name of Firing (title)
- Kiln being used (will be a reference to a KILN DATA schema later)
- Environmental -- ambient Temperature and humidity, weather of the day.
- Date / Time - Start
- Type of Firing
- Description of the attempted firing project
- Photos of the pre-work (TBD)
- Kiln Load Volume
- (future) Kiln Firing Schedule (will be a reference to a KILN FIRING SCHEDULE DATA schema later)
- (future) record the media being used and any special treatments (Clay, Glass, Glazing)

User can add content to this, make notes.

This should complete the first screen of data entry.

## Kiln Progress Screen

Saving the START LOAD form will then bring them back to a separate view showing a Step controlled form.  There may be several steps here (interface TBD) but the general idea is to track the sequence and progress of the firing. Also serves as a "in process" dashboard.

The first step (Kiln Load information) will be shown as complete (Green button?).  This step can be entered back into and more data collected if needed.  There is no "forms lockout" when the form is complete.

Subsequent steps may be indicated as "to be completed" and should be completed in Sequence.  The next step in play in the process may be put to a different color (EG: Blue).  Steps after the next one in sequence will not react if touched or selected.

At this time, the physical activity of the Kiln artist should be that they have possession of the kiln and are actively preparing the media for firing. 

## Start Firing

This can be kicked off by the user selecting a "start" button. Application status should change to "Kiln Firing In Process" kind of status alert.  The time that the Kiln firing started is recorded, and it is assumed the Kiln operator will start to follow the __Kiln Firing Schedule__. 

STOPing the app will end the firing schedule and then will escape to the __KILN REST__ sequence. This effectively aborts the kiln firing sequence at the recorded time. 

PAUSE is not implemented at this time. 

The first step in the firing sequence is presented and then acted upon.  Once the kiln operator performs the action, they can select COMPLETE, and the next step in the firing sequence starts.

Kiln firing sequences are time sensitive. They typically have a duration of time that is needed to pass, and then there is a task to be done at the end of that time. 

(Future) Some sequences may be triggered by a physical occurrence, such as a temperature being reached.  Initial versions of the app will track manual temperature readings.  Future versions may take into account computer / controller assisted kiln firing tracking. 

Following the Firing Schedule sequence list is an iterative process and will go onto the next step until all steps are complete.

## Firing Complete
After the last step in the firing schedule is complete, or of there is an cancel / abort, an END OF FIRING time is recorded.

## Kiln Rest
Once the firing schedule is complete and the kiln is turned off, another "kiln rest timer" starts which should be twice the duration of time that the kiln was fired for.  EG:  Kiln firing took 4.5 hours.  Kiln Rest time should take 9.0 hours.

(Future) using internal Kiln temperatures and external ambient temperatures, you could send out an alert when the temps have equalized.

## Load Examine
After the temperatures have equalized, the Kiln operator can open the kiln and start inspecting the effect of the firing. 

in this section, the user can take and attach pictures, create notes around what the experience was, and record final timings and temp settings. 

Notes of what was found, what was successful, what was not successful should be recorded to help align and or adjust firing schedules, media types, glazes, etc. 

## End
Once all recordings are complete the user can save the firing data and mark this load as complete. 

The user will then be taken back to the list of firings (landing page) for further actions. 

**== Phase 2 ==**

Story:  As a user of the app, I want to be able to select a past firing and pull up a report showing the actions and results of the firing.

Story: As the user of the App, I want to be able to go back and edit the content in a firing report. 
Story: As the user of the app, I want to be able to "Archive" data for one or more firings off.  Once the Archive of the data is done, I want to be able to delete the archived records from the active list of firings.

**== Phase 3 ==**

Story: As the user of the app, I want to be able to see a cone firing reference chart.

**== Phase 4 ==**

Story: As the user of the app, I want to be able to catalog my Kilns, use the cataloged entries to be able to select from my KILN LOAD FIRING screen the kiln that I will be using

Story: As the user of the app, I want to be able to see and edit a list of firing schedules.  These may be related by media type, cone firing rating of the media, glaze, etc.

**== FUTURE ==**

Maintenance logs for kilns  
	- Manufacturers  
	- Web Sites  
	- Parts lists  
	- Model and Serial Numbers  
	- Contacts  
	- Date of Purchase  
	- Cost of kiln  
	- Log entries for any maintenance on kilns  
		-- Open log entry  
		-- Closed Log entry  
		








