# Kiln Controller / Kiln Tracker #

This is a work in progress to start the creation of an app / collection of hardware needed to track the performance of a Kiln over a set of firings and lifespan.  Key high level activities:

* Kiln status and info (per kiln)
* firing info (per kiln)
* Lifecycle (per kiln) and costs tracking (per firing) for issues of maintenance and chargebacks to a project
* Maintenance schedules (Lifespan of elements, relays, kiln wash or repair updates, etc)
* Firing progress monitoring -- potentially using Arduino like controllers, involvement of a mobile app (for progress and alerts), possibly some remote work involving a hosted server for data collection, analysis, and push notifications. 
* Cone ratings to Temperature conversions / look ups (C & F)
* Tracking Firing Schedules and the related programming of that schedule per controller type (Autofire / Bartlett)

* Integration with Orton Autofire or Bartlett V6-CF's may come later. 

## Key Objects

### Kiln

Kilns vary by volume, shape, controller type, material type, media purpose, power draw, as well as physical configurations to account for different activities that you may do with the kiln.

Key attributes for the kiln would be:

|Attribute|Description|
|---------|-----------|
|Manufacturer| maker of the kiln itself|
|Model| Model Number of the kiln|
|Serial Number | Serial number of the kiln|
|Voltage | Typically 240V Single Phase for a larger ceramic kiln meant for residential use. 120V Single Phase may be for smaller kilns, whereas 208V three phase would be for a commercial site kiln installation|
|Amperage| Amperage requirements of the input power of the kiln. Smaller Kilns are 20-30 amps (again 120VAC), larger kilns may be between 45-70 Amps, and larger commercial kilns may go even higher|
| KWH Rating | KiloWatt per hour consumption (average?) of the kiln.  Optional content that a manufacturer may or may not provide.  Helps in calculating the operational cost of a firing|
| Manufacturer Contact Info| Support number for the kiln manufacturer in case of a need for support, parts, or advice|



### Firing Schedule
Firing schedules vary by types of media fired, and there may be some "Sub types" of firing schedules for a particular media depending on the purpose of the firing itself.  

For example, a Ceramic firing may be for a "Bisque" firing for when you have a raw clay object (greenware), and this is the first firing serves the purpose of hardening the object, drying it out, and preparing it for later stages of firing.  Later stages may be Glazing, Raku'ing, or another higher temperature firing to then convert the "clay" into a "ceramic state" (clay vitrification, quartz inversion, etc). 

For a Kiln that does not have a computerized controller, a Manual firing schedule will take place.  Timed events and instructions on where to set the kiln temperature at a particular time, along with any physical actions to be performed, are set out in a planned schedule and then followed per the time schedule manually by the artist / kiln operator until the firing is complete. 

For a kiln with a computerized controller, the kiln's controller will be pre-programmed with the steps needed to ramp up (or down) the temperature at a defined rate (T/hr) until a temperature is reached and then the "hold" (or Soak) time at that temperature.  Successive steps of "Ramp-Temp-Hold" can be used to help consistently perform a Glass fusing at the correct temperature for a specific formulation of glass, or a high fire glaze on Ceramic media, or anything in between.

The "firing schedule" itself should be thought of as a recipe or a set of repeatable steps for performing the same firing configuration for the content and load volume for a particular kiln.

Key attributes for the Firing Schedule would be:

|Attribute|Description|
|---------|-----------|
| Firing Schedule Title | |
| Media Used | | 
| Firing Type | | 
| Controller Used | {Manual, Orton Autofire, Bartlett V6-CF, etc} | 
| Notes | | 
| Firing Prep | | 
| Firing Sequence | | 
| (N) Step Number | | 
| (N) Ramp at T/Hr | | 
| (N) Target Temp | | 
| (N) Hold / Soak Time | | 

Notes:
* Firing Sequence is a list of Steps.  Each Step has 4 attributes { Step Number, Ramp Rate, Target Temp, Hold / Soak time } These correlate to the programming of the kiln controllers.  
* GLASS KILN CONTROLLERS typically have details around TEMPERATURE.  CERAMIC KILN CONTROLLERS typically have details around CONE values.  While there is a corollary and conversions between the CONE value and the TEMPERATURE, 

### Firing Event
This is the firing event in progress.  It would take into account the KILN and FIRING SCHEDULE and also add in information about the specific firing itself -- media, start times, end times, amount of load in the kiln, pre-fire conditions, post-fire conditions (before and after picts, results, etc)

### Firing History

## * * * Below to be edited later * * * ##

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact